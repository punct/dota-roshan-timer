import { writeFile } from 'fs'

const PREFIX = 'R0SH4N'
const NUMBER_OF_FILES = 60
const getFileName = (i = '') => `test${i}.cfg`

const WRITE_KEY = 'kp_plus'
const RESET_KEY = 'kp_minus'
const getButtons = () => [
    { key: 'KP_0', label: '0' },
    { key: 'KP_1', label: '1' },
    { key: 'KP_2', label: '2' },
    { key: 'KP_3', label: '3' },
    { key: 'KP_4', label: '4' },
    { key: 'KP_5', label: '5' },
    { key: 'KP_6', label: '6' },
    { key: 'KP_7', label: '7' },
    { key: 'KP_8', label: '8' },
    { key: 'KP_9', label: '9' },
]

const getFormattedTime = (timeComponents: string[]) => {
  const str = timeComponents.join('')
  return `${str.slice(0, -2)}:${str.slice(-2)}`
}

const addMinutes = (timeComponents: string[], minutes) => {
  const [mins, secs] = getFormattedTime(timeComponents)
    .split(':')
    .map(x => +x)
  const secsToAdd = minutes * 60

  const updatedSecs = secs + secsToAdd
  return `${Math.floor(mins + updatedSecs / 60)}:${updatedSecs % 60}`
}

const getCurrentAliasName = (): string =>
    `${PREFIX}_current`
const getResetAliasName = (): string =>
    `${PREFIX}_reset`
const getTimeAliasName = (timeComponents: string[]): string =>
    `${PREFIX}_${timeComponents.join('')}`

const generateBind = (key: string, command: string): string =>
    `bind "${key}" "${command}"`
const generateAlias = (name: string, command: string): string =>
    `alias "${name}" "${command}"`
const generateSay = (timeComponents: string[]): string => {
  const diedTime = getFormattedTime(timeComponents)
  const expireTime = addMinutes(timeComponents, 5)
  const respawnTimeMin = addMinutes(timeComponents, 8)
  const respawnTimeMax = addMinutes(timeComponents, 11)
  return `say_team "Died at ${diedTime}. Aegis expires at ${expireTime}. Respawns between ${respawnTimeMin}-${respawnTimeMax}"`
}

const generateAliases = (timeComponents: string[]): string[] => {
    if (timeComponents.length === 4) {
        const endCommand = `${generateAlias(getCurrentAliasName(), generateSay(timeComponents))}; ${getCurrentAliasName()}; ${getResetAliasName()}`
        return [generateAlias(getTimeAliasName(timeComponents), endCommand)]
    }

    if (timeComponents.length === 0) {
        const resetCommand = getButtons().map(button =>
            generateBind(button.key, getTimeAliasName([button.label]))
        ).join('; ')
        return [
            generateAlias(getResetAliasName(), resetCommand),
            ...getButtons().map(button => generateAliases([button.label]).join('\n')),
        ]
    }
  
    return [
        generateAlias(getTimeAliasName(timeComponents), getButtons().map(button => {
            const command = generateBind(button.key, getTimeAliasName([ ...timeComponents, button.label ]))
            return command
          }).join('; ')),
        ...getButtons().map(button => {
          return generateAliases([...timeComponents, button.label]).join('\n')
        })
    ]
}

const generateCfg = (): string => {
    return [
        ...generateAliases([]),
        getResetAliasName(),
    ].join('\n');
}

const main = () => {
  const cfg = generateCfg().split('\n')
  const blockSize = cfg.length / NUMBER_OF_FILES
  
  let files = []
  for (let i = 0; i < NUMBER_OF_FILES; i++) {
    files.push(getFileName(`${i}`))
  }
  const mainConfig = [
    ...files.map(fileName => `exec ${fileName}`),
    generateBind(RESET_KEY, getResetAliasName()),
    generateBind(WRITE_KEY, getCurrentAliasName()),
  ]
    .join('\n')


  files.forEach((fileName, index) => {
    const start = blockSize * index
    const end = blockSize * (index + 1)

    writeFile(fileName, cfg.slice(start, end).join('\n'), (error) => {
      if (error) {
        console.log(`Error saving to ${fileName}: ${error}`)
      }
    })
  })

  writeFile(getFileName(), mainConfig, error => {
    if (error) {
      console.log(`Error saving to ${getFileName()}: ${error}`)
    }
  })
}

main()
console.log('Config saved');
